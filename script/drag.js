const cards = document.getElementsByClassName('card');
let currentCard = null;

function onMouseMove(event) {
    if (currentCard && currentCard.dataset.dragging === '1') {
        const offsetX = event.clientX - currentCard.initialX;
        const offsetY = event.clientY - currentCard.initialY;

        // Calculate the maximum allowable positions
        const maxX = document.documentElement.clientWidth - currentCard.offsetWidth;
        const maxY = document.documentElement.clientHeight - currentCard.offsetHeight;

        // Update the card's position while constraining it within the document
        let newLeft = currentCard.initialLeft + offsetX;
        let newTop = currentCard.initialTop + offsetY;
        newLeft = Math.min(Math.max(0, newLeft), maxX);
        newTop = Math.min(Math.max(0, newTop), maxY);

        currentCard.style.left = `${newLeft}px`;
        currentCard.style.top = `${newTop}px`;
    }
}

for (let i = 0; i < cards.length; i++) {
    const titleBar = cards[i].querySelector('.titlebar');
    titleBar.addEventListener("mousedown", (event) => {
        currentCard = cards[i];
        currentCard.dataset.dragging = '1';
        const rect = currentCard.getBoundingClientRect();
        currentCard.initialX = event.clientX;
        currentCard.initialY = event.clientY;
        currentCard.initialLeft = rect.left;
        currentCard.initialTop = rect.top;
        currentCard.style.zIndex = 1;
        if (!currentCard.classList.contains("focused")) {
            currentCard.classList.add("focused");
        }
        for (let n = 0; n < cards.length; n++) {
            if (cards[n] != currentCard) {
                cards[n].style.zIndex = 0;
            }
        }
        document.addEventListener("mousemove", onMouseMove);
    });
    cards[i].addEventListener("mousedown", (event) => {
        currentCard = cards[i];
        currentCard.style.zIndex = 1;
        if (!currentCard.classList.contains("focused")) {
            currentCard.classList.add("focused");
        }
        for (let n = 0; n < cards.length; n++) {
            if (cards[n] != currentCard) {
                cards[n].style.zIndex = 0;
                if(cards[n].classList.contains("focused")) {
                    cards[n].classList.remove("focused");
                }
            }
        }
    });
}

window.addEventListener("mouseup", () => {
    if (currentCard) {
        currentCard.dataset.dragging = '0';
        currentCard = null;
        document.removeEventListener("mousemove", onMouseMove);
    }
});
