import UAParser from "ua-parser-js";
class Terminal {
    constructor(terminalElement, promptElement, contentElement, windowElement) {
        this.terminal = terminalElement;
        this.prompt = promptElement;
        this.content = contentElement;
        this.windowElem = windowElement;
        this.commands = [];
    }

    addCommand(name, description, args, onRun) {
        const command = new Command(name, description, args, onRun);
        this.commands.push(command);
    }

    init() {
        document.addEventListener("keydown", (event) => {
            const terminalFocused = this.terminal.style.zIndex == 1;
            if (terminalFocused) {
                const key = event.key;
                if (key == 'Enter') {
                    this.printToTerminal(`[user@cute-catgirl.gitlab.io]$ ${this.prompt.textContent}`);
                    this.executeCommand(this.prompt.textContent.trim());
                    this.prompt.innerHTML = "";
                } else if (key == 'Backspace') {
                    this.prompt.innerHTML = this.prompt.innerHTML.slice(0, -1);
                } else if (key != 'Shift' && key != 'Meta') {
                    this.prompt.innerHTML += key;
                }
            }
        });
    }

    executeCommand(commandString) {
        // Parse the command string and execute the corresponding command
        const [commandName, ...args] = commandString.split(" ");
        const command = this.commands.find(cmd => cmd.name === commandName);
        if (command) {
            command.onRun(args);
        } else {
            this.printToTerminal(`Command not found: ${commandName}`);
        }
    }

    printToTerminal(message) {
        // Append message to terminal content
        const messageElement = document.createElement("div");
        messageElement.textContent = message;
        this.content.appendChild(messageElement);
        this.windowElem.scrollTop = this.windowElem.scrollHeight;
    }
}

class Command {
    constructor(name, description, args, onRun) {
        this.name = name;
        this.description = description;
        this.args = args; // Store the arguments array
        this.onRun = onRun;
    }

    // Method to get a string representation of command usage
    getUsage() {
        if (this.args.length === 0) {
            return this.name;
        } else {
            return `${this.name} ${this.args.map(arg => `<${arg}>`).join(" ")}`;
        }
    }
}

const terminalElement = document.getElementById("terminal");
const promptElement = document.getElementById("promptcontent");
const contentElement = document.getElementById("terminalcontent");
const windowElement = document.querySelector("#terminal > .content")

const uap = new UAParser();
let userdetails;
let batterydetails;
userdetails = uap.getResult();
try {
    navigator.getBattery().then(function(data) {
        batterydetails = data;
    });
} catch {
    batterydetails = {
        level: 1,
        charging: true
    }
}

const ascii = `        ..@g@H..        
     Hg.@@.@@@...gH     
   .@@@.@H.H.....@H@@   
  .@@@@@@g......H@@@@@  
 ..@@@@@.........@.H.@@ 
...@@............@@@@@@@
.....@..........@@@@@@@H
 ...............@@@@@@. 
  .....@@@@........g@@  
   ....@@@@@@......@@   
     ...g@@@.......     
        .g......                         `;

const terminal = new Terminal(terminalElement, promptElement, contentElement, windowElement);

terminal.addCommand("echo", "Echoes the provided message to the terminal", ["message"], (args) => {
    terminal.printToTerminal(args.join(" "));
});

terminal.addCommand("help", "Displays a list of available commands", [], () => {
    terminal.printToTerminal("Available commands:");
    terminal.commands.forEach(command => {
        terminal.printToTerminal(`${command.getUsage()}: ${command.description}`);
    });
});

terminal.addCommand("neofetch", "Displays information about the user's system", [], () => {
    const asciiLines = ascii.split('\n'); // Split the ASCII art into lines
    const resolution = `${window.screen.width * window.devicePixelRatio}x${window.screen.height * window.devicePixelRatio}`
    const infoLines = [
        "user@cute-catgirl.gitlab.io",
        "---------------------------",
        `OS: ${userdetails.os.name}`,
        `Browser: ${userdetails.browser.name} ${userdetails.browser.version}`,
        `Engine: ${userdetails.engine.name} ${userdetails.engine.version}`,
        `Resolution: ${resolution}`,
        `Battery: ${batterydetails.level * 100}% (${batterydetails.charging ? "Charging" : "Discharging"})`
    ];

    const maxLength = Math.max(...infoLines.map(line => line.length)); // Calculate the maximum length of the info lines

    const lines = asciiLines.map((line, index) => {
        const infoLine = infoLines[index] || ''; // Get the corresponding info line or an empty string
        return `${line}  ${infoLine}`; // Combine the ASCII art line and the padded info line with spacing
    });
    terminal.printToTerminal('\n');
    lines.forEach(line => terminal.printToTerminal(line));
    terminal.printToTerminal('\n');
});

terminal.addCommand("reload", "Reloads the webpage", [], () => {
    location.reload();
});

terminal.init(); // Start listening for key events
