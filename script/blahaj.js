import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { RoomEnvironment } from 'three/addons/environments/RoomEnvironment.js';
import modelUrl from '../scene.glb';

const container = document.getElementById("blahajcontent");
const containerWindow = document.getElementById("blahaj");

const renderer = new THREE.WebGLRenderer();
renderer.setSize(300, 300);
container.appendChild(renderer.domElement);

const environment = new RoomEnvironment(renderer);
const pmremGenerator = new THREE.PMREMGenerator(renderer);

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(55, 1, 0.1, 1000);
scene.environment = pmremGenerator.fromScene(environment).texture;
environment.dispose();

const loader = new GLTFLoader();

let model;
loader.load(modelUrl, function (gltf) {
    model = gltf.scene;

    // Traverse the model and ensure all materials respond to lighting
    model.traverse(function (child) {
        if (child.isMesh) {
            if (!(child.material instanceof THREE.MeshStandardMaterial)) {
                child.material = new THREE.MeshStandardMaterial({
                    color: child.material.color || new THREE.Color(0xffffff),
                    map: child.material.map || null,
                    metalness: 0.5,
                    roughness: 0.5
                });
            }
            child.material.envMapIntensity = 1;
        }
    });

    model.position.y -= 0.05;
    scene.add(model);
});

camera.position.z = 0.5;

const ambient_light = new THREE.AmbientLight(0xFFFFFF, 1);
scene.add(ambient_light);

const color = 0xFFFFFF;
const intensity = 2;
const directional_light = new THREE.DirectionalLight(color, intensity);
directional_light.position.set(0, 2, 0.5);
directional_light.target.position.set(0, -0.05, 0);
scene.add(directional_light);
scene.add(directional_light.target);

function animate() {
    requestAnimationFrame(animate); // Ensure animation loop
    renderer.render(scene, camera);
}
animate();

window.addEventListener("mousemove", (event) => {
    if (model) {
        let rect = containerWindow.getBoundingClientRect();
        let mouseX = -(event.clientX - rect.left) / rect.width * 2 + 1;
        let mouseY = -(event.clientY - rect.top) / rect.height * 2 + 1;

        let mouseVector = new THREE.Vector3(mouseX, mouseY, 0.5);
        mouseVector.unproject(camera);

        let direction = mouseVector.sub(camera.position).normalize();
        direction.z = -direction.z;

        let angleY = Math.atan2(-direction.x, direction.z);
        let angleX = Math.asin(-direction.y);

        model.rotation.x = angleX;
        model.rotation.y = angleY;
    }
});
