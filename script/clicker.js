const title = document.getElementById('title');
const button = document.getElementById('click');
const card = document.getElementById('main');

let data;

button.addEventListener('click', () => {
    data.threes += 1;
    if (data.threes > data.threes_max) {
        data.threes = data.threes_max;
    }
    save();
    update();
});

function update() {
    if (data.threes >= 1) {
        title.innerHTML = "cute catgirl :" + "3".repeat(data.threes);
    } else {
        title.innerHTML = "cute catgirl"
    }
}

function save() {
    localStorage.setItem("clickData", JSON.stringify(data));
}

function load() {
    const defaultData = {
        "threes": 1,
        "threes_max": 10
    };
    data = Object.assign(defaultData, JSON.parse(localStorage.getItem("clickData")));
}

window.onload = (event) => {
    load();
    update();
}